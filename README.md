# naveira2

Naveira music version 2

## WebSocket services

We need a websocket service for signaling. Letting the peers know when and how to connect to new peers.

As an alternative to hosting our own solution, we can use a SaaS solution.

Here are some free alternatives

* https://www.ably.io
* https://fanout.io
* https://www.scaledrone.com
* https://pusher.com - For client messaging needs a private channel, which requires an auth server.

ably.io has the best free plan (100 concurrent connections) and a simple API https://www.ably.io/documentation/quick-start-guide#receiving-messages

