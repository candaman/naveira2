// @flow
import * as timesync from 'timesync';

import { ownId } from './ids';
import { sendToPeer, getPeerIds, subscribeToMethod } from './peer';

const syncedClock = timesync.create({
  peers: [], // start empty, will be updated at the start of every synchronization
  interval: 15000,
  delay: 5000,
  timeout: 10000
});

// Update the peer list before each time before syncing
syncedClock.on('sync', (state) => {
  if (state === 'start') {
    syncedClock.options.peers = getPeerIds();
  }
});

// Override timesync send method, and send the data to the webrtc peer instead
syncedClock.send = (id, data) => {
  sendToPeer('timesync', { id: ownId, data }, id);

  // Ignoring timeouts
  return Promise.resolve();
};

subscribeToMethod('timesync', ({ id, data }) => {
  if (!data.params) {
    data.params = null;
  }
  syncedClock.receive(id, data);
});

export default syncedClock;
