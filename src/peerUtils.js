export const getConnectedPeers = peer => Object.keys(peer.connections)
  .filter(id => peer.connections[id].some(conn => conn.open));

export const getPeerConnections = peer => Object.keys(peer.connections)
  .reduce((peerConnections, peerId) => [...peerConnections, ...peer.connections[peerId]], []);

export const getOpenConnections = peer => getPeerConnections(peer)
  .filter(connection => connection.open);
