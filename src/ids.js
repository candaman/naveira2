
import uuid from 'uuid/v4';
import queryString from 'query-string';

const searchRoomId = queryString.parse(window.location.search).roomId;
export const roomId = searchRoomId || uuid();

// const storedId = localStorage.getItem('ownId');
// export const ownId = storedId || uuid();
export const ownId = uuid();

if (!searchRoomId && window.history && window.history.replaceState) {
  window.history.replaceState(null, null, `?roomId=${roomId}`);
}

// if (!storedId) {
//   window.localStorage.setItem('ownId', ownId);
// }

