import React from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import styled from 'styled-components';
import dragDrop from 'drag-drop';
import WebTorrent from 'webtorrent';

import { roomId, ownId } from './ids';
import syncedClock from './syncedClock';
import { subscribeToMethod, sendToAllPeers, getPeerIds } from './peer';
import { playAudioAtTime, setMusicBuffer } from './music';

// 2 seconds to give time for everyone to receive the message that the song should play
const PLAY_DELAY = 2000;

const CopyLink = styled.span`
  color: blue;
  cursor: pointer;
  text-decoration: underline;
`;

const client = new WebTorrent();

class App extends React.Component {
  constructor(props) {
    super(props);

    subscribeToMethod('play-sound', ({ playTime }) => {
      playAudioAtTime(playTime, syncedClock);
    });

    this.state = {
      copied: false,
      logs: [],
      offset: null,
      fileName: null,
      progress: null
    };

    syncedClock.on('sync', () => {
      this.setState({
        offset: syncedClock.offset
      });
    });

    // Download file when someone wants to share it
    subscribeToMethod('share-file', ({ magnetURI }) => {
      console.log('adding torrent', magnetURI);
      client.add(magnetURI, (torrent) => {
        torrent.on('download', (bytes) => {
          console.log(`just downloaded: ${bytes}`);
          console.log(`total downloaded: ${torrent.downloaded}`);
          console.log(`download speed: ${torrent.downloadSpeed}`);
          console.log(`progress: ${torrent.progress}`);
          this.setState({
            progress: torrent.progress
          });
        });

        const file = torrent.files[0];

        this.setState({
          fileName: file.name
        });

        // Torrents can contain many files.
        file.getBuffer((err, buffer) => {
          console.log('setting music buffer');
          setMusicBuffer(buffer);
        });
      });
    });

    // When user drops files on the browser, create a new torrent and start seeding it!
    dragDrop('body', (files) => {
      client.seed(files, (torrent) => {
        console.log(`Client is seeding ${torrent.magnetURI}`);
        sendToAllPeers('share-file', {
          magnetURI: torrent.magnetURI
        }, true);

        const file = torrent.files[0];

        this.setState({
          fileName: file.name
        });

        file.getBuffer((err, buffer) => {
          console.log('setting music buffer');
          setMusicBuffer(buffer);
        });
      });
    });
  }

  playSound = () => {
    const playTime = syncedClock.now() + PLAY_DELAY;
    console.log('syncedClock.now()', syncedClock.now());
    console.log('playTime', playTime);

    sendToAllPeers('play-sound', {
      playTime
    });

    playAudioAtTime(playTime, syncedClock);
  }

  onCopy = () => {
    this.setState({
      copied: true
    });
    setTimeout(() => {
      this.setState({
        copied: false
      });
    }, 3000);
  }

  render() {
    const {
      copied,
      logs,
      offset,
      peers,
      fileName,
      progress
    } = this.state;

    const roomLink = `${window.location.host}?roomId=${roomId}`;

    return (
      <React.Fragment>
        <h1>Naveira 2</h1>
        <div>
          <div>Drag and drop a file here</div>
          <div>Share this link with your friends.</div>
          <div>{roomLink}</div>
          <CopyToClipboard
            text={roomLink}
            onCopy={this.onCopy}
          >
            <CopyLink>Copy to clipboard</CopyLink>
          </CopyToClipboard>
          {copied && <span> Copied!</span>}
        </div>
        <button type="button" id="play" onClick={this.playSound}>Play!</button>
        <div>{`Own id: ${ownId}`}</div>
        <div>{`Offset: ${offset}`}</div>
        <div>{`Peers: ${getPeerIds().join(', ')}`}</div>
        <div>{`File: ${fileName}`}</div>
        <div>{`Progress: ${Math.round(progress * 100)}`}</div>
        <div>
          {logs.map(log => <div>{log}</div>)}
        </div>
      </React.Fragment>
    );
  }
}

export default App;
